/*
 *  This software is free.
 */

package cardranker;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

class Card
{
    char rank_suit[] = {'A','d'};
    Image cardImg;
    int W,H,x0,y0;

    public Card()
    {
        W=H=x0=y0=0;
        cardImg = loadImage('d');
    }
    public Card(int x00,int y00, int W0, int H0,char rank, char suit)
    {
            x0 = x00;
            y0 = y00;
            W = W0;
            H = H0;
            cardImg = loadImage(suit);
            rank_suit[0] = rank;
            rank_suit[1] = suit;
    }

    public void draw(Graphics g)
    {
        Font font = new Font("Jokerman", Font.PLAIN, 26);
        g.setFont(font);
        g.drawImage(cardImg,x0,y0,W,H,null);
        if(rank_suit[1]=='d'||rank_suit[1]=='h')
        {
                g.setColor(Color.red);
        }
        else
        {
            g.setColor(Color.black);
        }
        String toDraw = Character.toString(Character.toUpperCase(rank_suit[0]));
        if(rank_suit[0]=='t') toDraw = "10";
        g.drawString(toDraw,x0+W/8,y0+H/5);
    }

    public void draw(Graphics g, int x, int y)
    {
        Font font = new Font("Jokerman", Font.PLAIN, W/4);
        g.setFont(font);
        g.drawImage(cardImg,x0+x,y0+y,W,H,null);
        if(rank_suit[1]=='d'||rank_suit[1]=='h')
        {
                g.setColor(Color.red);
        }
        else
        {
            g.setColor(Color.black);
        }
        String toDraw = Character.toString(Character.toUpperCase(rank_suit[0]));
        if(rank_suit[0]=='t')
        {
            toDraw = "10";
            g.drawString(toDraw,x+x0+W/8-5,y+y0+H/5);
        }
        else
        {
           g.drawString(toDraw,x+x0+W/8,y+y0+H/5); 
        }
        
        
    }    
    
    public Image loadImage(char c)
    {
        Image ret = null;
        String fName = "Images/";
        switch(c)
        {
            case 'h':
                fName = fName+"card_heart.png";
                break;
            case 's':
                fName = fName+"card_spade.png";
                break;
            case 'd':
                fName = fName+"card_diamond.png";
                break;
            case 'c':
                fName = fName+"card_club.png";
                break;
            default:
                fName = fName+"card_heart.png";
                break;                    
        }
        try
        {
            ret = ImageIO.read(new File(fName));

        } catch (IOException e)
        {
            System.out.println("Error loading screen images");
            e.printStackTrace();
        }
        return ret;
    }
    
    public void reload(char rank,char suit)
    {
        rank_suit[0] = rank;
        rank_suit[1] = suit;
        cardImg = loadImage(suit);
    }
    
    public static char intToSuit(int n)
    {
        char ret;
        switch(n)
        {
            case(1):
                ret = 'c';
                break;
            case(2):
                ret = 'd';
                break;
            case(3):
                ret = 'h';
                break;
            default:
                ret = 's';
                break;
        }
        return ret;
    }
    
    public static char intToRank(int n)
    {
        char ret = '\0';
        if(n>1&&n<10) ret = (char)(n+48);     
        else if(n>9&&n<15)
        {
            switch(n)
            {
                case(10):
                    ret = 't';
                    break;
                case(11):
                    ret = 'j';
                    break;
                case(12):
                    ret = 'q';
                    break;
                case(13):
                    ret = 'k';
                    break;
                case(14):
                    ret = 'a';
                    break;
                default:
                    break;            
            }
        }
        return ret;
    }
    
    public void setCoords(int x, int y)
    {
        x0 = x;
        y0 = y;
    }            
    
    public char rank()
    {
        return rank_suit[0];
    }
    public char suit()
    {
        return rank_suit[1];
    }
    public int rankToInt()
    {
        int ret = -1;
        int tmp = (int)rank_suit[0];
        if(tmp>49&&tmp<58) ret = tmp - 48;
        else
        {
            switch(tmp)
            {
                case 116:   // Jack
                    ret = 10;
                    break;
                case 106:   // Jack
                    ret = 11;
                    break;
                case 113:   // Queen
                    ret = 12;
                    break;
                case 107:   // King
                    ret = 13;
                    break;
                case 97:   // Ace
                    ret = 14;
                    break;                                        
            }
        }
        return ret;
    }
    public int suitToInt()
    {
        int ret = -1;
        switch(rank_suit[1])
        {
            case 'c':
                ret = 1;
                break;
            case 'd':
                ret = 2;
                break;
            case 'h':
                ret = 3;
                break;
            case 's':
                ret = 4;
                break; 
            default:
                break;
        }        
        return  ret;
    }
    
    public void copy(Card c)
    {
        rank_suit[0] = c.rank();
        rank_suit[1] = c.suit();
        cardImg = c.cardImg;
        W = c.W;
        H = c.H;
        x0 = c.x0;
        y0 = c.y0;
    }
    
    @Override
    public String toString()
    {
        String s1 = "";
        s1 = s1+Character.toString(rank())+Character.toString(suit());
        return s1;
    }
}