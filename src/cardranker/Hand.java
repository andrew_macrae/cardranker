package cardranker;

public class Hand
{
    int sz;
    int idx;
    boolean ready;
    public Card[] cards;
    int strength;
    boolean evaluated;
    int handRank;

    public Hand()
    {
        evaluated = false;
        sz = 5;
        idx = 0;
        ready = false;
        cards = new Card[sz];
        strength = 0;
        for (int i = 0; i < sz; i++)
        {
            cards[i] = new Card(0, 0, 0, 0, '0', '0');
        }
    }

    public void reset()
    {
        evaluated = ready = false;
        idx = 0;
    }

    public boolean addCard(Card c)
    {
        if (idx > 4)
        {
            System.out.println("Error: could not add card!");
            return false;
        } else
        {
            cards[idx].copy(c);
            idx++;
            if (idx == 5)
            {
                ready = true;
            }
            return true;
        }
    }
/* *********************************************
 *          Public Evaluation Methods          *
 * ******************************************* */
    public int handRank()
    {
        if(evaluated)
        {
            return handRank;
        }       
        else if (isStraightFlush())
        {
            handRank = 8;
            return handRank;
        } 
        else if (isQuads())
        {
            handRank = 7;
            return handRank;
        } 
        else if (isFullHouse())
        {
            handRank = 6;
            return handRank;

        } 
        else if (isFlush())
        {
            handRank = 5;
            return handRank;

        } 
        else if (isStraight())
        {
            handRank = 4;
            return handRank;
        }
        else if (isTrips())
        {
            handRank = 3;
            return handRank;

        }
        else if (isTwoPair())
        {
            handRank = 2;
            return handRank;
        } 
        else if (isPair())
        {
            handRank = 1;
            return handRank;
        } else
        {
            return 0;
        }
    }

    public static String handRankToString(int n)
    {
        switch (n)
        {
            case (0):
                return "High Card";
            case (1):
                return "One Pair";
            case (2):
                return "Two Pair";
            case (3):
                return "Three of a Kind";
            case (4):
                return "Straight";
            case (5):
                return "Flush";
            case (6):
                return "Full House";
            case (7):
                return "Four of a Kind";
            case (8):
                return "Straight Flush";
            default:
                return "Undefined Integer Code";
        }
    }    
    

    
/* *********************************************
 *       Helper funtions for evaluations       *
 * ******************************************* */
    public boolean sortByRank()
    {
        if (!ready)
        {
            return false;
        } else
        {
            Card tmp = new Card(0, 0, 0, 0, '0', '0');
            int lMax = cards.length;
            while (lMax > 1)
            {
                for (int i = 0; i < lMax - 1; i++)
                {
                    if (cards[i].rankToInt() > cards[i + 1].rankToInt())
                    {
                        tmp.copy(cards[i]);
                        cards[i].copy(cards[i + 1]);
                        cards[i + 1].copy(tmp);
                    }
                }
                lMax--;
            }
            return true;
        }
    }

    public boolean sortBySuit()
    {
        if (!ready)
        {
            return false;
        } else
        {
            Card tmp = new Card(0, 0, 0, 0, '0', '0');
            int lMax = cards.length;
            while (lMax > 1)
            {
                for (int i = 0; i < lMax - 1; i++)
                {
                    if (cards[i].suitToInt() > cards[i + 1].suitToInt())
                    {
                        tmp.copy(cards[i]);
                        cards[i].copy(cards[i + 1]);
                        cards[i + 1].copy(tmp);
                    }
                }
                lMax--;
            }
            return true;
        }
    }
    
    void copy(Hand h1)
    {
        sz = h1.sz;
        idx = h1.idx;
        ready = h1.ready;
        evaluated = h1.evaluated;
        strength = h1.strength;
        handRank = h1.handRank;
        for(int i = 0;i<idx;i++)
        {
            cards[i].copy(h1.cards[i]);
        }
    }

    /* *********************************************
     *                Evaluations                  *
     * ******************************************* */
    public boolean isStraight()
    {
// Things are much easier if the hand are sorted.        
        sortByRank();
        for (int i = 0; i < sz - 2; i++)
        {
            if (cards[i].rankToInt() != cards[i + 1].rankToInt() - 1)
            {
                return false;
            }
        }
// We only get here if the first 4 cards form a straight. Thn check the final card. Either ...       
        if ((cards[3].rankToInt() == cards[4].rankToInt() - 1) || // we have a conventional straight, ...
                (cards[3].rank() == '5' && cards[3].rank() == 'a')) // we have an A-5 straight, ...
        {
            return true;
        }
        // .. or we didn't hit the stright on the final card.
        return false;
    }

    public boolean isFlush()
    {
        sortBySuit();
        for (int i = 0; i < sz - 1; i++)
        {
            if (cards[i].suit() != cards[i + 1].suit())
            {
                return false;
            }
        }
        return true;
    }

    public boolean isQuads()
    {
        sortByRank();
        int cnt = 0;
        for (int i = 0; i < sz - 1; i++)
        {
            if (cards[i].rank() == cards[i + 1].rank())
            {
                cnt++;
            } else
            {
                cnt = 0;
            }
            if (cnt == 3)
            {
                return true;
            }
        }
        return false;
    }

    public boolean isFullHouse()
    {
        //if(isQuads()) return false;
        sortByRank();
        int diffcnt = 0;
        for (int i = 0; i < sz - 1; i++)
        {
            if (cards[i].rank() != cards[i + 1].rank())
            {
                diffcnt++;
            }
        }
        if (diffcnt < 2)
        {
            return true;
        } else
        {
            return false;
        }
    }

    public boolean isTrips()
    {
        //if(isQuads()||isFullHouse()) return false;
        sortByRank();
        int cnt = 0;
        for (int i = 0; i < sz - 1; i++)
        {
            if (cards[i].rank() == cards[i + 1].rank())
            {
                cnt++;
            } else
            {
                cnt = 0;
            }
            if (cnt == 2)
            {
                return true;
            }
        }
        return false;
    }

    public boolean isTwoPair()
    {
        //if(isQuads()||isFullHouse()||isTrips()) return false;
        sortByRank();
        int numPairs = 0;
        for (int i = 0; i < sz - 1; i++)
        {
            if (cards[i].rank() == cards[i + 1].rank())
            {
                numPairs++;
            }
        }
        if (numPairs > 1)
        {
            return true;
        } else
        {
            return false;
        }
    }

    private boolean isPair()
    {
        //if(isQuads()||isFullHouse()||isTrips()||isTwoPair()) return false;
        sortByRank();
        for (int i = 0; i < sz - 1; i++)
        {
            if (cards[i].rank() == cards[i + 1].rank())
            {
                return true;
            }
        }
        return false;
    }

    private boolean isStraightFlush()
    {
        return (isStraight() && isFlush());
    }

/* *****************************************************
 * Helper functions for comparing equal strength hands *
 * *************************************************** */

// Player with the highest card wins. If they have the same highest card, compare the next highest ...
// if all cards have same rank, they draw   
    public int compareHiCards(Hand opponent)
    {
        sortByRank();
        opponent.sortByRank();
        for(int i = 0;i<sz;i++)
        {
            if(cards[i].rankToInt()>opponent.cards[i].rankToInt())
            {
                return 1;
            }
            if(cards[i].rankToInt()<opponent.cards[i].rankToInt())
            {
                return -1;
            }
        }
        return 0;
    }
    
    public int compareOnePair(Hand h1)
    {
        int p1 = getPair();
        int p2 = h1.getPair();
        
        if(p1>p2) return 1;
        else if(p2>p1) return -1;
        else
        {
            // at this point, hands are sorted, find the highest non-suited hand:
            int heroKickers[] = new int[3];
            int villainKickers[] = new int[3];
            int idxHero = 0;
            int idxVillain = 0;
            for(int i = 4;i>=0;i--)
            {
                if(cards[i].rankToInt()!=p1)
                {
                    heroKickers[idxHero] = cards[i].rankToInt();
                    idxHero++;
                }
                if(h1.cards[i].rankToInt()!=p2)
                {
                    villainKickers[idxVillain] = h1.cards[i].rankToInt();
                    idxVillain++;
                }                
            }
            for(int i = 0;i<heroKickers.length;i++)
            {
                if(heroKickers[i]>villainKickers[i])
                {
                    return 1;
                }
                else if(heroKickers[i]<villainKickers[i])
                {
                    return -1;
                }
            }
            // only reached if dead draw
            return 0;
        }
    }
    
    public int getPair()
    {     
        sortByRank();        
        for (int i = 0; i < sz - 1; i++)
        {
            if (cards[i].rank() == cards[i + 1].rank())
            {
                return cards[i].rankToInt();
            }
        }
        return -1;
    }
    private int compareThreeOfaKind(Hand h1)
    {
        int heroRank = getTrips();
        int villainRank = h1.getTrips();
        
        if(heroRank>villainRank) return 1;
        else if(heroRank<villainRank) return -1;
        else
        {
            // at this point, hands are sorted, find the highest non-suited hand:
            int heroKickers[] = new int[2];
            int villainKickers[] = new int[2];
            int idxHero = 0;
            int idxVillain = 0;
            for(int i = 4;i>=0;i--)
            {
                if(cards[i].rankToInt()!=heroRank)
                {
                    heroKickers[idxHero] = cards[i].rankToInt();
                    idxHero++;
                }
                if(h1.cards[i].rankToInt()!=villainRank)
                {
                    villainKickers[idxVillain] = h1.cards[i].rankToInt();
                    idxVillain++;
                }                
            }
            for(int i = 0;i<heroKickers.length;i++)
            {
                if(heroKickers[i]>villainKickers[i])
                {
                    return 1;
                }
                else if(heroKickers[i]<villainKickers[i])
                {
                    return -1;
                }
            }            
            return 0;
        }
    }
    
    public int getTrips()
    {
        sortByRank();        
        int cnt = 0;
        for (int i = 0; i < sz - 1; i++)
        {
            if (cards[i].rank() == cards[i + 1].rank())
            {
                cnt++;
            } else
            {
                cnt = 0;
            }
            if (cnt == 2)
            {
                return cards[i].rankToInt();
            }
        }
        return -1;        
    }
    public int compareTwoPair(Hand h1)
    {
        int p1a = -1,p1b = -1;
        int p2a = -1,p2b = -1;
        
        sortByRank();
// Find pair ranks        
        for (int i = 0; i < sz - 1; i++)
        {
            if (cards[i].rank() == cards[i + 1].rank())
            {
                if(p1a<0) p1a = cards[i].rankToInt();
                else p1b = cards[i].rankToInt();
            }
            if (h1.cards[i].rank() == h1.cards[i + 1].rank())
            {
                if(p2a<0) p2a = h1.cards[i].rankToInt();
                else p2b = h1.cards[i].rankToInt();
            }
        }
        
        if(p1a > p2a) return 1;
        else if(p1a < p2a) return -1;
        else if(p1b > p2b) return 1;
        else if(p1b < p2b) return -1;
        else
        { // compare kicker
            int heroKicker = -99,villainKicker = -99;
            for(int i = 4;i>=0;i--)
            {
                if(cards[i].rankToInt()!=p1a && cards[i].rankToInt()!=p1b) heroKicker = cards[i].rankToInt();
                if(h1.cards[i].rankToInt()!=p2a && h1.cards[i].rankToInt()!=p2b) villainKicker = h1.cards[i].rankToInt();
            }
            if(heroKicker>villainKicker) return 1;
            else if(heroKicker<villainKicker) return -1;
            else return 0;
        }
    }    
    
    public int compare(Hand h1)
    {
        if(handRank()>h1.handRank)
        {
            return 1;
        }
        else if(handRank()<h1.handRank())
        {
            return -1;
        }
        else
        {
            switch(handRank())
            {
                case(0):
                    return compareHiCards(h1);
                case(1):
                    return compareOnePair(h1);
                case(2):
                    return compareTwoPair(h1);                    
                case(3):
                    return compareThreeOfaKind(h1);                    
                case(4):
                    return compareStraight(h1);                    
                case(5):
                    return compareFlush(h1);                    
                case(6):
                    return compareFullHouse(h1);                    
                case(7):
                    return compareFourOfaKind(h1);                    
                case(8):
                    return compareFlush(h1);                    
                    
                default:
                    System.out.println("Error in Hand.compare() ...");
                    return -2;
            }
                
        }
    }
    
    
    public int compareStraight(Hand h1)
    {
        sortByRank();
        h1.sortByRank();
        if(cards[4].rankToInt()>h1.cards[4].rankToInt()) return 1;
        else if(cards[4].rankToInt()<h1.cards[4].rankToInt()) return -1;
        else return 0;
    }
    public int compareFlush(Hand h1)
    {
        return compareHiCards(h1);
    }
    
    public int compareFullHouse(Hand h1)
    {
        int hero2=0,hero3=0;
        int villain2=0,villain3=0;
        sortByRank();        
        int cnt = 0;
        for (int i = 0; i < sz - 1; i++)
        {
            if (cards[i].rank() == cards[i + 1].rank())
            {
                cnt++;
            } 
            else
            {
                if(cnt == 2) 
                {
                    hero2 = cards[i].rankToInt();
                    hero3 = cards[i+1].rankToInt();
                    break;
                }
                else
                {
                    hero3 = cards[i].rankToInt();
                    hero2 = cards[i+1].rankToInt();
                    break;
                }
            }
        }
        cnt = 0;
        for (int i = 0; i < sz - 1; i++)
        {
            if (h1.cards[i].rank() == h1.cards[i + 1].rank())
            {
                cnt++;
            } 
            else
            {
                if(cnt == 2)
                {
                    villain2 = h1.cards[i].rankToInt();
                    villain3 = h1.cards[i+1].rankToInt();
                    break;
                }
                else
                {
                    villain3 = h1.cards[i].rankToInt();
                    villain2 = h1.cards[i+1].rankToInt();
                    break;
                }
            }
        }

        if(hero3 > villain3) return 1;
        else if(hero3 < villain3) return -1;
        else if(hero2 > villain2) return 1;
        else if(hero2 < villain2) return -1;
        else return 0;
    }
    
    public int compareFourOfaKind(Hand h1)
    {
        int hero4 = getQuads();
        int villain4 = h1.getQuads();
        
        if(hero4>villain4) return 1;
        if(hero4<villain4) return -1;
        else
        { // compare kicker
            int heroKicker = -99,villainKicker = -99;
            for(int i = 4;i>=0;i--)
            {
                if(cards[i].rankToInt()!=hero4) heroKicker = cards[i].rankToInt();
                if(h1.cards[i].rankToInt()!=villain4) villainKicker = h1.cards[i].rankToInt();
            }
            if(heroKicker>villainKicker) return 1;
            else if(heroKicker<villainKicker) return -1;
            else return 0;           
        }
    }
    
    public int getQuads()
    {
        sortByRank();        
        int cnt = 0;
        for (int i = 0; i < sz - 1; i++)
        {
            if (cards[i].rank() == cards[i + 1].rank())
            {
                cnt++;
            } else
            {
                cnt = 0;
            }
            if (cnt == 3)
            {
                return cards[i].rankToInt();
            }
        }
        return -1;                
    }
    
    @Override
    public String toString()
    {
        String s1 = "";
        for(int i = 0;i<5;i++)
        {
            s1 = s1+cards[i].toString()+",";
        }
        return s1;
    }
}