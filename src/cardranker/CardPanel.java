package cardranker;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 * @author Andrew MacRae <liboff@gmail.com>
 *
 *
 */
public class CardPanel extends JPanel
{

    int[] heroStrength = new int[21];
    int[] villainStrength = new int[21];

    int[][] mask =
    {
        {
            1, 1, 1, 1, 1, 0, 0
        },
        {
            1, 1, 1, 1, 0, 1, 0
        },
        {
            1, 1, 1, 0, 1, 1, 0
        },
        {
            1, 1, 0, 1, 1, 1, 0
        },
        {
            1, 0, 1, 1, 1, 1, 0
        },
        {
            0, 1, 1, 1, 1, 1, 0
        },
        {
            1, 1, 1, 1, 0, 0, 1
        },
        {
            1, 1, 1, 0, 1, 0, 1
        },
        {
            1, 1, 0, 1, 1, 0, 1
        },
        {
            1, 0, 1, 1, 1, 0, 1
        },
        {
            0, 1, 1, 1, 1, 0, 1
        },
        {
            1, 1, 1, 0, 0, 1, 1
        },
        {
            1, 1, 0, 1, 0, 1, 1
        },
        {
            1, 0, 1, 1, 0, 1, 1
        },
        {
            0, 1, 1, 1, 0, 1, 1
        },
        {
            1, 1, 0, 0, 1, 1, 1
        },
        {
            1, 0, 1, 0, 1, 1, 1
        },
        {
            0, 1, 1, 0, 1, 1, 1
        },
        {
            1, 0, 0, 1, 1, 1, 1
        },
        {
            0, 1, 0, 1, 1, 1, 1
        },
        {
            0, 0, 1, 1, 1, 1, 1
        }
    };
    int[] strengths = new int[21];
    Card[] heroHole, villainHole, community;
    Hand heroHand, villainHand;
    Deck deck;

    Image bgImage, rArrowImage, lArrowImage;

    int bgW, bgH;

    int playerWins;
    int heroHandStrength, villainHandStrength;

    boolean tabulaRasa;

    public CardPanel()
    {
        tabulaRasa = true;
        heroHand = new Hand();
        villainHand = new Hand();
        deck = new Deck();
        heroHole = new Card[2];
        villainHole = new Card[2];
        community = new Card[5];
        for (int i = 0; i < 2; i++)
        {
            heroHole[i] = new Card();
            villainHole[i] = new Card();
        }
        for (int j = 0; j < 5; j++)
        {
            community[j] = new Card();
        }

        deck.shuffle();
        try
        {
            bgImage = ImageIO.read(new File("Images/board_bg.png"));
            rArrowImage = ImageIO.read(new File("Images/right_arrow.png"));
            lArrowImage = ImageIO.read(new File("Images/left_arrow.png"));

        } catch (IOException e)
        {
            System.out.println("Error loading background image");
            e.printStackTrace();
        }
        bgW = bgH = 256;

        playerWins = 0;
    }

    @Override
    public void paintComponent(Graphics g)
    {
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                g.drawImage(bgImage, i * 256, j * 256, this);
            }
        }
        Font font = new Font("Jokerman", Font.PLAIN, 26);
        g.setFont(font);

        if (tabulaRasa)
        {
            g.setColor(Color.yellow);
            g.drawString("Press 'd' to deal!", 300, 150);
            tabulaRasa = false;
            
        } else
        {
            for (int i = 0; i < heroHole.length; i++)
            {
                heroHole[i].draw(g, 25 + i * (10 + heroHole[0].W + 5), 20);
                villainHole[i].draw(g, 600 + i * (10 + heroHole[0].W + 5), 20);
            }
            for (int j = 0; j < community.length; j++)
            {
                community[j].draw(g, 140 + j * (10 + community[0].W + 5), 220);
            }
            g.setColor(Color.YELLOW);
            g.drawString(Hand.handRankToString(heroHand.handRank()), 80, 200);
            g.drawString(Hand.handRankToString(villainHand.handRank()), 655, 200);

            if (heroHand.compare(villainHand) < 0)
            {
                g.drawImage(rArrowImage, 290, 50, this);
            } else if (heroHand.compare(villainHand) > 0)
            {
                g.drawImage(lArrowImage, 290, 50, this);
            } else
            {
                g.drawString("DRAW!!!!", 370, 60);
            }
        }
    }

    public void handleInput(char code, KeyEvent e)
    {
        switch (code)
        {
            case 'p': // Key Pressed
                if (e.getKeyCode() == KeyEvent.VK_UP)
                {
                }
                break;
            case 'r':  // Released
                break;
            case ('t'): // Typed

                char c = Character.toLowerCase(e.getKeyChar());
                switch (c)
                {
                    case 'f':
                        break;
                    case 'd':
                        deal();
                        break;
                    case 't':
                }
                break;
        }
    }

    public void handleMouse(char code, MouseEvent e)
    {
        switch (code)
        {
            case 'd':
                repaint();
                break;
            case 'p':
                break;
            case 'r':
                break;
            case 'c':
                System.out.println("(" + e.getX() + "," + e.getY() + ")");
        }
    }

    public void deal()
    {
        deck.shuffle();
        for (int i = 0; i < 2; i++)
        {
            heroHole[i].copy(deck.deal());
            villainHole[i].copy(deck.deal());
        }
        for (int j = 0; j < 5; j++)
        {
            community[j].copy(deck.deal());
        }
        villainHand.copy(evaluate(append(villainHole, community)));
        heroHand.copy(evaluate(append(heroHole, community)));
        repaint();
    }

    public Hand evaluate(Card[] cards)
    {
        if (cards.length != 7)
        {
            System.out.println("Error ... incorrect number of hands, brah!");
            System.exit(-1);
        }
        Hand hand = new Hand(); // temporary holding hand
        Hand ret = new Hand();
        int mx = -1;
        for (int i = 0; i < strengths.length; i++)
        {
            hand.reset();
            for (int j = 0; j < mask[0].length; j++)
            {
                if (mask[i][j] == 1)
                {
                    hand.addCard(cards[j]);
                }
            }
            strengths[i] = hand.handRank();

            if (strengths[i] > mx)
            {
                mx = strengths[i];
                ret.copy(hand);
            } else if (strengths[i] == mx)
            {
                if (hand.compare(ret) > 0)
                {
                    ret.copy(hand);
                }
            }
        }
        return ret;
    }

    public Card[] append(Card[] c1, Card[] c2)
    {
        int lTot = c1.length + c2.length;
        Card[] ret = new Card[lTot];
        for (int i = 0; i < c1.length; i++)
        {
            ret[i] = new Card();
            ret[i].copy(c1[i]);
        }
        for (int i = c1.length; i < lTot; i++)
        {
            ret[i] = new Card();
            ret[i].copy(c2[i - c1.length]);
        }
        return ret;
    }
}
